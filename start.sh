cd site
docker build -t site .
docker run -d -p 8080:8080 --restart always site

cd ../haproxy/
docker build -t haproxy .
docker run -d --net=host --restart always haproxy
https://blog.georgejose.com/moving-my-http-website-to-https-using-letsencrypt-haproxy-and-docker-deb56ff6be9b


1. Crete EC2 on AWS (ubuntu)
2. Set up Elastic IP and bind it to EC2 
3. Buy domain on www.namecheap.com
4. Get free certificate for domain from https://letsencrypt.org/ 
5. Write simple node app
6. Write docker image with node app 
7. Write docker image for HAProxy
8. Configure HAProxy

```
cat /etc/letsencrypt/live/mirza-demo.site/fullchain.pem /etc/letsencrypt/live/mirza-demo.site/privkey.pem | tee /home/ubuntu/haproxy/private/mirza-demo.site.pem
```

```
sudo apt-get update
curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker ubuntu
docker --version
sudo apt install npm
sudo apt install letsencrypt
sudo npm install -g http-server
mkdir ssl/
sudo letsencrypt certonly --webroot -w ssl/ -d mirza-demo.site
git clone https://mirza_abazovic@bitbucket.org/mirza_abazovic/haproxy-example.git

```
Result after ```sudo letsencrypt certonly --webroot -w ssl/ -d mirza-demo.site```

```
ubuntu@ip-172-31-32-209:~/haproxy-example/site$ sudo letsencrypt certonly --webroot -w ssl/ -d mirza-demo.site
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator webroot, Installer None
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for mirza-demo.site
Using the webroot path /home/ubuntu/haproxy-example/site/ssl for all unmatched domains.
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/mirza-demo.site/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/mirza-demo.site/privkey.pem
   Your cert will expire on 2019-08-01. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

```